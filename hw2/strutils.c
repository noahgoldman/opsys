/*
 * Noah Goldman, 661058951
 */

#include "string.h"
#include "stdlib.h"
#include "stdio.h"
#include "ctype.h"

#include "strutils.h"

char* strep(const char *input, const char *to_replace, const char *replace_with)
{
  char* p_index = strstr(input, to_replace);
  int index = p_index - input;

  char* second_half = p_index + strlen(to_replace);

  int len = strlen(input) - strlen(to_replace) + strlen(replace_with) + 1;
  char *output = (char*)malloc(len * sizeof(char));

  // Copy the beginning over to the new string
  strncpy(output, input, index);
  // Null-terminate the string to avoid valgrind warnings
  output[index] = 0;

  fprintf(stderr, "the input string is %s, the string to replace is %s and the replacement string is %s\n",
      output, to_replace, replace_with);

  // Append the replacing string
  strcat(output, replace_with);
  strcat(output, second_half);

  return output;
}

int check_substr(const char *input, const char *substr)
{
  return strstr(input, substr) == NULL ? 0 : 1;
}

void strtrim(char *input)
{
  char *str = input;

  while (isspace(*str)) {
    str++;
  }

  if (*str == '\0') {
    *input = '\0';
  }

  char *str_end = input + strlen(input) - 1;
  while (isspace(*str_end)) {
    str_end--;
  }

  *(str_end + 1) = '\0';

  char *new_str = strdup(str);

  input = new_str;
}

int strcount(const char *input, const char *search_for)
{
  int c = 0;
  const char *itr = input;
  while ((itr = strstr(itr, search_for))) {
    itr++;
    c++;
  }

  return c;
}
