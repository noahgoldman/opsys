/*
 * Noah Goldman, 661058951
 */

#include "stdio.h"
#include "string.h"
#include "stdlib.h"

#include "vector.h"

void print_history(Vector *v) {
  int i = 0;
  for (i = 0; i < v->size; i++) {
    printf("%03d  %s\n", i, v->val[i]);
  }
}

void add_history(Vector *v, char *str) {
  vector_add(v, str); 
}

char* get_cmd(Vector *v, const char *cmd_prefix) {
  char *prefix;
  char *copy = strdup(cmd_prefix);
  prefix = copy + 1;

  int i;
  for (i = v->size - 1; i >= 0; i--) {
    char *cmd = v->val[i];
    if (strstr(cmd, prefix) == cmd) {
      char *output = (char*)malloc(strlen(cmd) * sizeof(char));
      strcpy(output, cmd); 
      return output;
    }
  }

  return NULL;
}
