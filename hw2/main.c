/* 
 * Noah Goldman, 661058951
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <regex.h>
#include <assert.h>

#include "vector.h"
#include "path.h"
#include "history.h"
#include "strutils.h"

#define ENV "MYPATH"
#define CMD_SIZE 200
#define HOME_DIR "~"
#define HOME "HOME"
#define TEMP_STREAM 3
#define WILDCARD "*"

Vector *history;

int check_prefix_match(char *prefix, char *str)
{
  return strstr(str, prefix) == str;
}

void terminate_str(char *cmd)
{
  int len = strlen(cmd);
  if (len > 0 && cmd[len - 1] == '\n') {
    cmd[len - 1] = '\0';
  }
}

int get_stream_id(char *pipe_str)
{
    return strcmp(pipe_str, "<==") == 0 ? 0 : 1;
}

int check_valid_type(unsigned char type)
{
  return type == DT_REG || type == DT_LNK;
}

void escape_regex_chars(char *str)
{
  if (strstr(str, ".") != NULL) {
    str = strep(str, ".", "\\.");
  }
}

char* get_regex_string(char *str)
{
  int len = strlen(str) + 3;
  char *new_str = calloc(len, sizeof(char));

  strcat(new_str, "^");
  strcat(new_str, str);
  strcat(new_str, "$");

  escape_regex_chars(new_str);
  return new_str;
}

void expand_wildcards(Vector *v)
{
  int i, first = 1;
  Vector *params = vector_copy(v);
  for (i = 1; i < params->size; i++) {
    fprintf(stderr, "looped! with i=%d and v->size=%d\n", i, v->size);
    char *arg = params->val[i];
    if (arg != NULL && strstr(arg, WILDCARD) != NULL) {
      regex_t file_regex;
      char *arg_rep = strdup(arg);
      char *regex = get_regex_string(arg_rep);

      // Replace wildcards with the proper regex
      int count = strcount(arg_rep, WILDCARD);
      int k;
      for (k = 0; k < count; k++) {
        arg_rep = strep(regex, "*", ".*");
      }

      int rc = regcomp(&file_regex, arg_rep, 0);
      if (rc) {
        fprintf(stderr, "ERROR: Could not compile regex\n");
        return;
      }

      DIR *dir = opendir("."); 
      struct dirent *entry;
      while ((entry = readdir(dir))) {
        rc = regexec(&file_regex, entry->d_name, 0, NULL, 0);
        if (rc == 0) {
          fprintf(stderr, "found match %s\n", entry->d_name);
          if (first) {
            v->val[i] = strdup(entry->d_name);
            first = 0;
          } else {
            vector_push_after(v, entry->d_name, i);
          }
        }
      }
    }
  }
}

int redirect(int id, char *file, int append)
{
  fflush(stdout);
  fflush(stdin);
  dup2(id, TEMP_STREAM);
  close(id);
  int flags;
  if (id == 0) {
    flags = O_RDONLY;
  } else if (append) {
    flags = O_WRONLY | O_APPEND;
  } else { 
    flags = O_WRONLY | O_CREAT | O_TRUNC;
  }
  int rc = open(file, flags, 0600);
  return rc;
}

void reset_redirect(int id)
{
  dup2(TEMP_STREAM, id);
}

Vector* parse_cmd(const char *cmd)
{
  Vector *v = malloc(sizeof(Vector));
  vector_init(v);

  char *p, *str = strdup(cmd);
  p = strtok(str, " ");
  while (p != NULL) {
    vector_add(v, p);
    p = strtok(NULL, " ");
  }

  free(str);
  return v;
}

Vector* parse_env(const char* str)
{
  char *env;
  if (str != NULL) {
    env = strdup(str);
  }

  Vector *v = malloc(sizeof(Vector));
  vector_init(v);

  // Handle if the path variable is not set
  if (str == NULL) {
    char *c = "/usr/bin";
    vector_add(v, c);
  } else {
    char *p;
    p = strtok(env, ":");
    while (p != NULL) {
      vector_add(v, p);
      p = strtok(NULL, ":");
    }
  }

  return v;
}

char* check_path_cmd(char *path, const char *cmd)
{
  DIR *dir = opendir(path);

  if (dir == NULL) {
    perror("Failed to open the directory specified in the path variable");
    exit(1);
  }

  struct dirent *entry;
  while ((entry = readdir(dir))) {
    if (strcmp(entry->d_name, cmd) == 0 && check_valid_type(entry->d_type)) {
      return join_path(2, path, cmd);
    }
  }
  
  closedir(dir);
  return NULL;
}

char* get_cmd_path(const char *cmd)
{
  const char *val = getenv(ENV);
  Vector *paths = parse_env(val);

  int i;
  for (i = 0; i < paths->size; i++) {
    char *str = paths->val[i];
    char *result = check_path_cmd(str, cmd);
    if (result != NULL) {
      return result;
    }
  }

  return NULL;
}

int call(const char *path, Vector *v)
{
  int background = 0;

  int i;
  for (i = 0; i < v->size; i++) {
    fprintf(stderr, "%s\n", v->val[i]);
  }

  if (strcmp(v->val[v->size - 1], "&") == 0) {
    background = 1;
    v->val[v->size - 1] = NULL;
    v->size--;
  }

  pid_t pid = fork();

  if (pid < 0) {
    perror("Failed to fork a new process");
    return EXIT_FAILURE;
  }

  // child process
  if (pid == 0) {
    vector_null_terminate(v);
    execv(path, v->val);

    perror("exec() failed");
    return EXIT_FAILURE;
  } else {
    if (background) {
      fprintf(stderr, "[process running in background with pid %d]\n", pid);
    } else {
      int status;
      wait(&status);

      if (WIFSIGNALED(status)) {
        fprintf(stderr, "process terminated odly\n");
      }
    }
  }
      
  return EXIT_SUCCESS;
}

int run_command(const char *full_cmd)
{
  Vector *v = parse_cmd(full_cmd);

  expand_wildcards(v);

  char *cmd = v->val[0];
  int redirect_stream = -1;

  if (full_cmd[0] == '\0') {
    return 1;
  }

  if (strstr(full_cmd, HOME_DIR) != NULL) {
    char *home = getenv(HOME); 
    char* new_cmd = strep(full_cmd, HOME_DIR, home);

    v = parse_cmd(new_cmd);
  }

  if (v->size >= 3) {
    char *pipe_str = v->val[v->size - 2];
    if (strcmp(pipe_str, "<==") == 0 || strcmp(pipe_str, "==>") == 0 ||
        strcmp(pipe_str, "-->") == 0) {
      redirect_stream = get_stream_id(pipe_str);
      int append = strcmp(pipe_str, "-->") == 0;
      int rc = redirect(redirect_stream, v->val[v->size-1], append);

      if (rc < 0) {
        if (append) {
          fprintf(stderr, "No such file to append to\n");
        } else {
          fprintf(stderr, "Failed to open file\n");
        }

        reset_redirect(redirect_stream);
        return 1;
      }

      v->val[v->size - 2] = '\0';
    }
  }

  if (strcmp(cmd, "exit") == 0 || strcmp(cmd, "quit") == 0) {
    printf("bye\n");
    return 0;
  } else if (strcmp(cmd, "history") == 0) {
    print_history(history);
    return 1;
  } else if (strcmp(cmd, "cd") == 0) {
    chdir(v->val[1]);
    return 1;
  } else if (check_prefix_match("!", cmd)) {
    cmd = get_cmd(history, full_cmd);
    if (cmd == NULL) {
      fprintf(stderr, "ERROR: no command with '%s' prefix in history", cmd);
      return 1;
    }
    
    v = parse_cmd(cmd);
  } else if (strstr(full_cmd, "&") && strcmp(v->val[v->size - 1], "&") != 0) {
    fprintf(stderr, "ERROR: the '&' character can only be used at the end of a command\n");
    return 1;
  }


  char* path = get_cmd_path(cmd);

  if (path == NULL) {
    fprintf(stderr, "ERROR: command '%s' not found\n", cmd);
  } else {
    call(path, v);
  }

  if (redirect_stream != -1) {
    reset_redirect(redirect_stream);
  }

  free(path);
  vector_free(v);
  return 1;
}

int main()
{
  char cmd[CMD_SIZE];
  int result = 1;

  history = malloc(sizeof(Vector));
  vector_init(history);

  while (result) {
    printf("myshell$ ");
    fgets(cmd, CMD_SIZE, stdin);
    terminate_str(cmd);
    strtrim(cmd);

    add_history(history, cmd);

    result = run_command(cmd);
  }

  vector_free(history);
  return EXIT_SUCCESS;
}
