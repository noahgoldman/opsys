/*
 * Noah Goldman, 661058951
 */

#ifndef VECTOR_H_
#define VECTOR_H_

typedef struct {
  char **val;
  int size;
  int max;
} Vector;

void vector_init(Vector *v);
void vector_add(Vector *v, char *str);
void vector_free(Vector *v);
void vector_push_after(Vector *v, char *str, int push_after);
void vector_null_terminate(Vector *v);
Vector* vector_copy(Vector *v);

#endif // VECTOR_H_
