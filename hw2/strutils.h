/*
 * Noah Goldman, 661058951
 */

#ifndef STRUTILS_H_
#define STRUTILS_H_

char* strep(const char *input, const char *to_replace, const char *replace_with);
int check_substr(const char *input, const char *substr);
void strtrim(char *input);
int strcount(const char *input, const char *search_for);

#endif // STRUTILS_H_
