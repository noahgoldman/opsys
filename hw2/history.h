/*
 * Noah Goldman, 661058951
 */

#ifndef HISTORY_H_
#define HISTORY_H_

struct Vector;

void print_history(Vector *v);
void add_history(Vector *v, char *str);
char* get_cmd(Vector *v, const char *prefix);

#endif // HISTORY_H_
