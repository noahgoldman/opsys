# Tom D'Alba, Noah Goldman, John Martin

from simulator import context_switch, get_next_process, simulator, check_all_cpus_processing, get_arguments, get_time

def assign_RR( cpus, processes ):
    if len(processes) == 0:
        return

    for cpu in cpus:


        if cpu.process == None and not processes[0].running:
            context_switch( cpu, cpu.process, processes[0] )
            p = processes.pop( 0 )
            processes.append( p )

        elif cpu.process == None and processes[0].running:
            continue

        elif cpu.process.rr_switch == get_time() and not processes[0].running:

            context_switch( cpu, cpu.process, processes[0] )
            p = processes.pop( 0 )
            processes.append( p )

if __name__ == '__main__':
    n, m, part2, rrslice = get_arguments()
    simulator(n, m, assign_RR, part2, rrslice)
