# Tom D'Alba, Noah Goldman, John Martin

from simulator import context_switch, get_next_process, simulator, check_all_cpus_processing, get_arguments
from time import sleep

def assign_sjf_preemptive(cpus, processes):
    sorted_processes = sorted(processes, key=lambda x: x.burst_time)
    for cpu in cpus:
        if cpu.process is None:
            process = get_next_process(sorted_processes)
            if process is not None:
                context_switch(cpu, cpu.last_process, process)
    
    if check_all_cpus_processing(cpus):
        sorted_cpus = sorted(cpus, key=lambda x: x.process.burst_time)
        sorted_cpus.reverse() # We want to go from highest to soonest finish times

        waiting_processes = [x for x in sorted_processes if not x.running]

        if waiting_processes:
            for cpu in cpus:
                process = get_next_process(waiting_processes)
                if process and process.burst_time < cpu.process.burst_time: 
                    old_process = cpu.process
                    context_switch(cpu, old_process, process)

if __name__ == '__main__':
    m, n, part2, rrslice = get_arguments()
    simulator(m, n, assign_sjf_preemptive, part2, rrslice)

#change priority to burst_time
