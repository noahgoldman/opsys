from simulator import simulator, get_arguments

from fcfs import assign_fcfs
from sjf import assign_sjf
from preemptive_sjf import assign_sjf_preemptive
from round_robin import assign_RR
from preemptive_priority import assign_PP

if __name__ == '__main__':
    n, m, part2, rr_slice = get_arguments()
    print "\nFCFS:"
    simulator(n, m, assign_fcfs, part2, rr_slice);

    print "\nSJF:"
    simulator(n, m, assign_sjf, part2, rr_slice);

    print "\nPreemtive SJF:"
    simulator(n, m, assign_sjf_preemptive, part2, rr_slice);

    print "\nRound Robin:"
    simulator(n, m, assign_RR, part2, rr_slice);

    print "\nPreemptive Priority:"
    simulator(n, m, assign_PP, part2, rr_slice);
