# Tom D'Alba, Noah Goldman, John Martin

from simulator import get_next_process, simulator, get_arguments, context_switch

def assign_sjf(cpus, processes):
    processes.sort(key=lambda x: x.burst_time)
    for cpu in cpus:
        if cpu.process == None:
            cpu.process = get_next_process(processes)
            if cpu.process != None:
                context_switch(cpu, cpu.last_process, cpu.process)

if __name__ == '__main__':
    m, n, part2, rrslice = get_arguments()
    simulator(m, n, assign_sjf, part2, rrslice)
