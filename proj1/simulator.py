# Tom D'Alba, Noah Goldman, John Martin

import random
import string
import argparse
import sys
import math

def get_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument("-m", help="The number of CPUs", type=int, default=1)
    parser.add_argument("-n", help="The number of processes to create", type=int, default=20)
    parser.add_argument("-PART2", help="If set, the processes will arrive after time=0", action='store_true', default=False)
    parser.add_argument("-rrslice", help="Time slice for the red robin algorithm", type=int, default=100)
    args = parser.parse_args()
    return args.m, args.n, args.PART2, args.rrslice

def get_time():
    return g_time

def get_random_time():
    n = 8001
    while n > 8000:
        n = int(random.expovariate(0.001))
    return n

class Process:
    def __init__(self, pid, entry_time):
        self.burst_time = random.randint(50, 400)
        self.priority = random.randint(0,4)
        self.entry_time = entry_time
        self.start_time = None
        self.current_start_time = None
        self.finish_time = None
        self.rr_switch = None
        self.running = False
        self.finished = False
        self.pid = pid

    def turnaround(self):
        return self.finish_time - self.entry_time

    def initial_wait(self):
        return self.start_time - self.entry_time

    def total_wait(self):
        total = self.finish_time - self.entry_time - self.burst_time
        if self.start_time != 0:
            total -= context_start_time
        return total

    def finish(self):
        return self.finish_time + context_finish_time 

class CPU:
    def __init__(self, name):
        self.name = name
        self.process = None
        self.last_process = None


def check_finished(processes):
    for process in processes:
        if not process.finished:
            return False
    return True

def check_all_cpus_processing(cpus):
    for cpu in cpus:
        if cpu.process is None:
            return False
    return True

def get_next_process(processes):
    for process in processes:
        if process.finished == False and process.running == False:
            return process
    return None

def init_processes(n, part2, processes):
    split = int(math.ceil(n * 0.1)) if part2 else n
    split += 1
    for i in range(1, split):
        process = Process(i, 0)
        processes.append(process)
    for i in range(split, n+1):
        process = Process(i, get_random_time())
        processes.append(process)

def init_cpus(m, cpus):
    for i in range(0, m):
        cpu = CPU(string.uppercase[i])
        cpus.append(cpu)

def reset_last_process(cpus):
    for cpu in cpus:
        cpu.last_process = None

def context_switch(cpu, old, new):
    if new.start_time is None:
        new.start_time = g_time

    new.running = True
    if g_time is 0:
        new.current_start = g_time
    else:
        new.current_start = g_time + context_start_time
    new.rr_switch = g_time + context_switch_time + g_rr_slice

    cpu.process = new

    if old is not None:
        new.finish_time = g_time + new.burst_time + context_switch_time

        if g_time < old.current_start:
            old.burst_time = old.finish_time - old.current_start
        else:
            old.burst_time = old.finish_time - g_time

        old.running = False
        assert old.running is False

        print_context_switch(cpu, old, new)
    else:
        if not g_time is 0:
            new.finish_time = g_time + new.burst_time + context_start_time
            print_context_switch_noswap(cpu, new)
        else:
            new.finish_time = g_time + new.burst_time


def sum_processes(processes, func):
    total = 0
    for process in processes:
        total += func(process)
    return total

def print_create(process):
    print "[time %r] Process %r created (requires %rms CPU time)" % (
            g_time, process.pid, process.burst_time)

def print_finished(process, cpu):
    print "[time %r] Process %r completed its CPU burst on CPU %r (turnaround time %rms, initial wait time %rms, total wait time %rms)" % (
        g_time, process.pid, cpu.name, process.turnaround(), process.initial_wait(), process.total_wait())

def print_context_switch(cpu, old, new):
    print "[time %r] Context switch (swapping out process %r for process %r in CPU %r)" % (
            g_time, old.pid, new.pid, cpu.name)

def print_context_switch_noswap(cpu, new):
    print "[time %r] Context switch (inserting process %r into CPU %r)" % (
            g_time, new.pid, cpu.name)

def print_stats_line(processes, name, func):
    min_time = func(min(processes, key=func))
    max_time = func(max(processes, key=func))

    total = sum_processes(processes, func)
    avg = total / float(len(processes))

    print "%s time: min %.3f ms; avg %.3f ms; max %.3f ms" % (
            name, min_time, avg, max_time)

def print_statistics(m, processes):
    print_stats_line(processes, "Turnaround", (lambda x: x.turnaround()))
    print_stats_line(processes, "Initial wait", lambda x: x.initial_wait())
    print_stats_line(processes, "Total wait", lambda x: x.total_wait())

def simulator( m, n, algorithm, part2, rr_slice):
    master_processes = []
    init_processes(n, part2, master_processes)

    global g_rr_slice 
    g_rr_slice = rr_slice

    cpus = []
    init_cpus(m, cpus)
        
    global g_time
    global context_switch_time # the time for a full context switch
    global context_start_time # The time just to start a process, not the full switch
    global context_finish_time
    g_time = 0
    context_switch_time = 15
    context_start_time = 7
    context_finish_time = 8

    processes = []

    while(True):
        for process in master_processes:
            if process.entry_time == g_time:
                processes.append( process )
                print_create(process)

        algorithm(cpus, processes)

        # Clear all of the cpu.last_process fields
        reset_last_process(cpus)

        g_time += 1

        for cpu in cpus:
            process = cpu.process
            if process == None:
                continue

            if g_time == process.finish():
                process.finished = True
                process.running = False
                cpu.process = None
                cpu.last_process = process
                processes.remove(process)
                print_finished(process, cpu)

        if check_finished(master_processes):
            break # End the main loop of the algorithm

    print_statistics(m, master_processes)
