/*
 * Noah Goldman, 661058951
 */

#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <pthread.h>

#include "thread.h"

#define THREADS 10

// The id of the main socket
static int main_sock;

// Handle Ctrl->C
void signal_handler()
{
	close(main_sock);
	exit(EXIT_SUCCESS);
}

void args_error()
{
	printf("ERROR: Invalid Arguments\nUSAGE: file-server <port-number> <storage-directory>\n");
	exit(EXIT_FAILURE);
}

int cast_arg_to_int(char *str)
{
	char *endptr;
	errno = 0;

	int res = strtol(str, &endptr, 10);
	if ((errno != 0 && res == 0) ||
			(endptr != str + strlen(str))) {
		return -1;
	}

	return res;
}

int count_files(DIR *dir)
{
	int sum = 0;
	struct dirent *entry;
	while ((entry = readdir(dir))) {
		if (entry->d_type == DT_REG) {
			++sum;
		}
	}

	return sum;
}

void server(int port)
{
	main_sock = socket(PF_INET, SOCK_STREAM, 0);
	struct sockaddr_in server, client;
	struct hostent *host;

	if (main_sock < 0) {
		perror("Failed to open socket");	
		exit(EXIT_FAILURE);
	}

	server.sin_family = AF_INET;
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	server.sin_port = htons(port);

	if (bind(main_sock, (struct sockaddr*)&server, sizeof(server)) < 0) {
		perror("Failed to bind to socket");
		exit(EXIT_FAILURE);
	}

	int client_len_int = sizeof(client);
	socklen_t *client_len = (socklen_t*) &client_len_int;
	listen(main_sock, 5);

	while (1) {
		int newsock = accept(main_sock, (struct sockaddr*)&client, client_len);

		host = gethostbyaddr(&(client.sin_addr.s_addr), sizeof client.sin_addr.s_addr, AF_INET);
		if (host == NULL) {
			perror("Failed to get hostname");
		} else {
			printf("Recieved incoming connection from %s\n", host->h_name);
		}

		struct thread_args *args = (struct thread_args*)malloc(sizeof(struct thread_args));
		args->sock = newsock;

		pthread_create(&args->tid, NULL, handler, args);
		pthread_detach(args->tid);
	}
}

int main(int argc, char *argv[])
{
	char output[1000] = "Started File Storage Server\n";
	char file_line[255];

	if (argc != 3) {
		args_error();
	}

	int port = cast_arg_to_int(argv[1]);
	if (port < 8000 || port > 9000) {
		args_error();
	}

	DIR *dir = opendir(argv[2]);

	if (dir == NULL) {
		mkdir(argv[2], S_IRWXU | S_IRWXG | S_IRWXO);
		dir = opendir(argv[2]);
		sprintf(file_line, "Storage directory \"%s\" created successfully\n", argv[2]);
	} else {
		int files = count_files(dir);
		char *file_str = files == 1 ? "file" : "files";
		sprintf(file_line, "Storage directory \"%s\" already exists (contains %d %s)\n",
				argv[2], files, file_str);		
	}

	// Change to our storage directory now that it definitely exists
	int rc = chdir(argv[2]);
	if (rc < 0) {
		perror("Failed to change to the working directory");
	}

	// Make the line for the port
	char port_line[255];
	sprintf(port_line, "Listening on port %d\n", port);

	strcat(output, file_line);
	strcat(output, port_line);
	printf("%s", output);

	signal(SIGINT, signal_handler);

	// Start the main server
	server(port);

	return EXIT_SUCCESS;
}
