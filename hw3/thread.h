/*
 * Noah Goldman, 661058951
 */

#ifndef _HANDLER_H_
#define _HANDLER_H_

#include <dirent.h>

#define STORE 1
#define APPEND 2
#define DIRLIST 3
#define RETRIEVE 4

#define PATH "."

struct thread_args {
	unsigned long int tid;
	int sock;
};

struct command {
  int type;
  char *file;
  int size;
  char *data;
};

void *handler(void *arg);

#endif // _HANDLER_H_
