import socket
import sys
import time
import argparse
from threading import Thread
import threading

script, port, filename = sys.argv

def start_threads():
    for i in xrange(8):
        t = Thread(target=send_file)
        t.start()

def send_file():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(("localhost", int(port)))

    f = open(filename, 'rw')
    contents = f.read()

    cmd = "APPEND %s %d %s" % ("main.c", len(contents), contents)
    s.send(cmd)
    print str(threading.current_thread().ident) + " i want a append response..."
    print s.recv(10000)

    cmd = "RETRIEVE main.c"
    s.send(cmd)
    print str(threading.current_thread().ident) + " i want a retrieve response..."
    s.recv(100000)

    s.close()
    print str(threading.current_thread().ident) + " im done!"

start_threads()
