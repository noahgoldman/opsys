/*
 * Noah Goldman, 661058951
 */

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <dirent.h>
#include <assert.h>
#include <sys/ioctl.h>
#include <errno.h>

#include "thread.h"
#include "strutils.h"
#include "vector.h"

#define CMD_BUF_SIZE 1000
#define CLEAR_BUF_SIZE 1000

void init_cmd(struct command *cmd)
{
	cmd->type = 0;
	cmd->file = NULL;
	cmd->size = 0;
	cmd->data = NULL;
}

void remove_newline(char *str)
{
	// Delete the newline character from the filename
	if (str[strlen(str) - 1] == '\n') {
		// Null terminate
		str[strlen(str) - 1] = '\0';
	}

	strtrim(str);
}

void terminate_str(char *str, int maxlen)
{
	int i;
	for (i = 0; i < maxlen; i++) {
		if (str[i] == '\n') {
			str[i] = 0;
			return;
		}
	}
}

int get_type_from_str(char *str) {
	if (strcmp(str, "STORE") == 0) {
		return STORE;
	} else if (strcmp(str, "APPEND") == 0) {
		return APPEND;
	} else if (strcmp(str, "DIRLIST") == 0) {
		return DIRLIST;
	} else if (strcmp(str, "RETRIEVE") == 0) {
		return RETRIEVE;
	} 

	return 0;
}

void flush_socket(struct thread_args *args)
{
	int count;
	char buf[CLEAR_BUF_SIZE];
	ioctl(args->sock, FIONREAD, &count);
	while (count > 0) {
		recv(args->sock, buf, CLEAR_BUF_SIZE, 0);
		ioctl(args->sock, FIONREAD, &count);
	}
}

char* get_thread_msg(struct thread_args *args, const char *msg, int send)
{
	char prefix[100];
	if (send) {
		sprintf(prefix, "[thread %lu] Sent: ", args->tid);
	} else {
		sprintf(prefix, "[thread %lu] ", args->tid);
	}

	int msg_len = strlen(prefix) + strlen(msg) + 2;
	char *fmsg = malloc(msg_len * sizeof(char));

	strcpy(fmsg, prefix);
	strcat(fmsg, msg);
	strcat(fmsg, "\n");

	return fmsg;
}

void print_msg(struct thread_args *args, const char *msg)
{
	printf("%s", get_thread_msg(args, msg, 0));
}

void send_msg(struct thread_args *args, const char *msg)
{
	char *fmsg = get_thread_msg(args, msg, 1);
	printf("%s", fmsg);

	int len = strlen(msg);

	int rc = send(args->sock, msg, len, 0);
	if (rc < len) {
		perror("Failed to write the entire message");
	}
}

void send_error_msg(struct thread_args *args, const char *msg)
{
	send_msg(args, msg);
	flush_socket(args);
}

int check_file_exists(const char *filename)
{
	assert(strlen(filename) > 0);
	DIR *dir = opendir(PATH);

	struct dirent *res;

	struct dirent *entry = malloc(offsetof(struct dirent, d_name) +
			pathconf(PATH, _PC_NAME_MAX) + 1);
	
	while (readdir_r(dir, entry, &res) == 0 &&
			res != NULL) {
		if (strcmp(entry->d_name, filename) == 0) {
			free(entry);
			closedir(dir);
			return 1;
		}
	}

	free(entry);
	closedir(dir);
	return 0;
}

Vector* get_matching_files(const char *prefix)
{
	Vector *v = malloc(sizeof(Vector));
	vector_init(v);

	DIR *dir = opendir(PATH);
	if (dir == NULL) {
		return NULL;
	}

	struct dirent *res;
	struct dirent *entry = malloc(offsetof(struct dirent, d_name) +
			pathconf(PATH, _PC_NAME_MAX) + 1);

	while (readdir_r(dir, entry, &res) == 0 &&
			res != NULL) {
		if ((prefix == NULL ||
				(prefix != NULL && strstr(entry->d_name, prefix) == entry->d_name)) &&
				strcmp(entry->d_name, ".") != 0 &&
				strcmp(entry->d_name, "..") != 0) {
			vector_add(v, entry->d_name);	
		}
	}

	return v;
}

char* create_heap_str(const char *str)
{
	char *heap_str = malloc((strlen(str) + 1) * sizeof(char));
	strcpy(heap_str, str);
	return heap_str;
}

char* write_file(struct command *cmd, struct thread_args *args) {
	int flags = 0;
	char *error = create_heap_str("ERROR");
	
	if (cmd->type == STORE) {
		flags = O_WRONLY | O_CREAT | O_TRUNC;
	} else { // cmd->type == APPEND
		flags = O_WRONLY | O_APPEND;
	}

	int fd = open(cmd->file, flags, 0600);
	if (fd < 0) {
		send_error_msg(args, error);
		return NULL;
	}

	int rc = write(fd, cmd->data, cmd->size);
	if (rc < 0) {
		send_error_msg(args, error);
		return NULL;
	}

	return create_heap_str("ACK");
}

int run_cmd(struct thread_args *args)
{
	char *cmd_buf = calloc(CMD_BUF_SIZE, sizeof(char));
	struct command *cmd = (struct command*)malloc(sizeof(struct command));
	init_cmd(cmd);

	char msg_buf[10000];

	// Parse the base of the command
	int rc = recv(args->sock, cmd_buf, CMD_BUF_SIZE - 1, MSG_PEEK);

	// Check if the message is a disconnect message
	if (rc == 0) {
		return 0;
	} else if (rc < 0) {
		perror("Recv failed");
		return 0;
	}

	// Parse the type of cmd
	char *p;
	p = strtok(cmd_buf, " ");	

	remove_newline(p);
	cmd->type = get_type_from_str(p);
	if (cmd->type == 0) {
		send_error_msg(args, "ERROR");
		return 1;
	}

	// Parse the filename
	p = strtok(NULL, " ");
	if (p == NULL) {
		cmd->file = NULL;
	} else {
		cmd->file = malloc(strlen(p) * sizeof(char) + 1);
		strcpy(cmd->file, p);
	}

	if (cmd->type == STORE || cmd->type == APPEND) {
		// Get the length of the data
		p = strtok(NULL, " ");

		char *end;
		cmd->size = (int) strtol(p, &end, 10);
		if (end == p || errno == ERANGE) {
			send_error_msg(args, "ERROR");
			return 1;
		}

		// Calculate the length of the command before the data,
		// read that off of the stream, then read the rest of the data
		int cmd_len = p + strlen(p) - cmd_buf + 1;
		rc = recv(args->sock, cmd_buf, cmd_len, 0);
		if (rc < 0) {
			send_error_msg(args, "ERROR");
			return 1;
		}
		
		// Null terminate the string
		cmd_buf[cmd_len - 1] = 0;

		sprintf(msg_buf, "Rcvd: %s", cmd_buf);
		print_msg(args, msg_buf);

		// Check if the file exists or shouldn't exist
		int exists = check_file_exists(cmd->file) ;
		if (exists && cmd->type == STORE) {
			send_error_msg(args, "FILE EXISTS");
			return 1;
		} else if (!exists && cmd->type == APPEND) {
			send_error_msg(args, "NO SUCH FILE");
			return 1;
		}

		cmd->data = malloc(cmd->size * sizeof(char));

		rc = recv(args->sock, cmd->data, cmd->size, 0);
		if (rc < 0) {
			send_error_msg(args, "ERROR");
			return 1;
		}
		flush_socket(args);

		sprintf(msg_buf, "Transferred %d bytes", cmd->size);
		print_msg(args, msg_buf);

		char *res = write_file(cmd, args);
		if (res == NULL) {
			return 1;
		}
		send_msg(args, res);
		free(res);
	} else if (cmd->type == RETRIEVE) {
		remove_newline(cmd->file);
		
		int rc = recv(args->sock, cmd_buf, CMD_BUF_SIZE - 1, 0);
		if (rc < 0) {
			send_error_msg(args, "ERROR");
			return 1;
		}
		flush_socket(args);
		remove_newline(cmd_buf);

		sprintf(msg_buf, "Rcvd: %s", cmd_buf);
		print_msg(args, msg_buf);

		if (!check_file_exists(cmd->file)) {
			send_error_msg(args, "NO SUCH FILE");
			return 1;
		}

		// Get the file contents
		FILE *file = fopen(cmd->file, "r");
		
		if (!file) {
			send_error_msg(args, "ERROR");
			return 1;
		}	

		fseek(file, 0, SEEK_END);
		unsigned long size = ftell(file);
		rewind(file);

		char *buf = calloc(size + 100, sizeof(char));
		unsigned long res_size = fread(buf, sizeof(char), size, file);
		if (res_size != size) {
			send_error_msg(args, "ERROR");
			return 1;
		}
		fclose(file);
		
		buf[size] = '\0';

		char *out = malloc((size + 100) * sizeof(char));
		int len = sprintf(out, "ACK %lu %s", size, buf);
		rc = send(args->sock, out, len, 0);
		if (rc < 0) {
			send_error_msg(args, "ERROR");
			return 1;
		}
		sprintf(msg_buf, "Sent: ACK %lu", size);
		print_msg(args, msg_buf);

		sprintf(msg_buf, "Transferred file (%lu bytes)", size);
		print_msg(args, msg_buf);
		free(buf);
		free(out);
	} else if (cmd->type == DIRLIST) {
		// Read the rest of the message off the buffer
		recv(args->sock, cmd_buf, CMD_BUF_SIZE - 1, 0);
		flush_socket(args);

		terminate_str(cmd_buf, CMD_BUF_SIZE);
		sprintf(msg_buf, "Rcvd: %s", cmd_buf);
		print_msg(args, msg_buf);

		if (cmd->file != NULL) {
			remove_newline(cmd->file);
		}

		Vector *files = get_matching_files(cmd->file);
		if (files == NULL) {
			send_error_msg(args, "ERROR");
			return 1;
		}

		sprintf(msg_buf, "ACK %d ", files->size);
		if (files->size > 0) {
			strcat(msg_buf, files->val[0]);
			strcat(msg_buf, " ");
		}

		int i;
		for (i = 1; i < files->size; i++) {
			strcat(msg_buf, files->val[i]);
			strcat(msg_buf, " ");
		}

		send_msg(args, msg_buf);
	}

	free(cmd_buf);
	free(cmd);
	return 1;
}

void* handler(void *arg)
{
	// Get the socket that we are using
	struct thread_args *args = (struct thread_args*)arg;

	while (run_cmd(args)) { }

	close(args->sock);
	free(args);
	return NULL;
}
