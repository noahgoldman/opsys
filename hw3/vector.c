/*
 * Noah Goldman, 661058951
 */

#include "stdlib.h"
#include "string.h"
#include "stdio.h"

#include "vector.h"

#define INC_CONSTANT 20

void copy_string(char *in, char *out)
{
  int len = strlen(in);
  out = malloc(len * sizeof(char));
  strncpy(out, in, len);
}

void vector_resize(Vector *v)
{
  if (v->max - v->size == 0) {
    v->max = v->max + INC_CONSTANT;
    v->val = (char**)realloc(v->val, v->max * sizeof(char*));
  }
}

void vector_init(Vector *v)
{
  v->val = (char**)malloc(INC_CONSTANT * sizeof(char*));
  v->size = 0;
  v->max = INC_CONSTANT;
}

void vector_add(Vector *v, char *str)
{
  vector_resize(v); 

  int len = strlen(str);
  v->val[v->size] = calloc(len + 1, sizeof(char));
  strcpy(v->val[v->size++], str);
}

void vector_null_terminate(Vector *v)
{
  vector_resize(v);

  v->val[v->size++] = NULL;
}

void vector_push_after(Vector *v, char *str, int push_after)
{
  vector_resize(v);

  // Push down all the elements
  int i;
  for (i = push_after + 2; i <= v->size; i++) {
    v->val[i] = v->val[i-1];
  }

  v->val[push_after + 1] = calloc(strlen(str) + 1, sizeof(char));
  strcpy(v->val[push_after + 1], str);

  ++v->size;
}

void vector_free(Vector *v)
{
  int i;
  for (i = 0; i < v->size; i++) {
    free(v->val[i]);
  }

  free(v->val);
  free(v);
}

Vector* vector_copy(Vector *v)
{
  Vector *new_v = (Vector*)malloc(sizeof(Vector));
  vector_init(new_v);

  int i;
  for (i = 0; i < v->size; ++i) {
    vector_add(new_v, v->val[i]);
  }

  return new_v;
}
