/*
 * Noah Goldman, 661058951
 */

#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>

#include "path.h"
#include "stats.h"

// Build the mode string
char* get_mode_string(mode_t mode)
{
  char* mode_str = (char*)malloc(11 * sizeof(char));

  char dash = '-';
   
  // Set the first char for directory or symlink

  // Check if symlink
  if (S_ISLNK(mode)) {
    mode_str[0] = 'l';
  } else if (S_ISDIR(mode)) {
    mode_str[0] = 'd';
  } else {
    mode_str[0] = dash;
  }

  // Set the owner bits
  mode_str[1] = mode & S_IRUSR ? 'r' : dash;
  mode_str[2] = mode & S_IWUSR ? 'w' : dash;
  mode_str[3] = mode & S_IXUSR ? 'x' : dash;

  // Set the group bits
  mode_str[4] = mode & S_IRGRP ? 'r' : dash;
  mode_str[5] = mode & S_IWGRP ? 'w' : dash;
  mode_str[6] = mode & S_IXGRP ? 'x' : dash;

  // Set the other bits
  mode_str[7] = mode & S_IROTH ? 'r' : dash;
  mode_str[8] = mode & S_IWOTH ? 'w' : dash;
  mode_str[9] = mode & S_IXOTH ? 'x' : dash;

  mode_str[10] = '\0';

  return mode_str;
}

char* get_size_str(struct stat *stats)
{
  if (S_ISDIR(stats->st_mode))
  {
    return "subdirectory";
  }

  return "";
}

// Main function for printing statistics
void print_statistics(char *path, char *file)
{
  char *full_path = join_path(2, path, file);
  struct stat *stats = (struct stat*)malloc(sizeof(struct stat));

  if (lstat(full_path, stats)) {
    perror("stat failed");
    exit(1);
  }

  char* mode = get_mode_string(stats->st_mode);

  printf(" (");
  if (S_ISDIR(stats->st_mode)) {
    print_buffer(pbuf, "subdirectory) ");
  } else {
    char *out = NULL;
    sprintf(out, "%jd bytes) ", stats->st_size);
    print_buffer(pbuf, out);
  }
  char *mode_str = NULL;
  sprintf(mode_str, "[%s]\n", mode);
  print_buffer(pbuf, mode_str);

  free(stats);
  free(full_path);
  free(mode);
}
