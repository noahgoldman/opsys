/*
 * Noah Goldman, 661058951
 */

#ifndef STATS_H_
#define STATS_H_

#include "buffered_print.h"

char* get_mode_string(mode_t mode);

void print_statistics(char *path, char *file, print_buffer *pbuf);

#endif // STATS_H_
