#ifdef BUFFERED_PRINT_H_
#define BUFFERED_PRINT_H_

typedef struct print_buffer {
  char *start;
  char *cur;
  char *end;
} print_buffer;

void init_buffer(print_buffer* buf);
void add_buffer(print_buffer* buf, char *str);
void flush_buffer(print_buffer* buf);
void clear_buffer_to(print_buffer *pbuf, char *clear_to);

#endif // BUFFERED_PRINT_H_
