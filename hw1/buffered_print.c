#include "buffered_print.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"

void init_buffer(print_buffer *pbuf)
{
	int size = 100;
	pbuf->start = (char*)malloc(size * sizeof(char));
	pbuf->cur = pbuf->start;
	pbuf->end = pbuf->start + size;
	pbuf->cur = pbuf->start;
	*pbuf->start = '\0';
}

void add_buffer(print_buffer *pbuf, char *str)
{
	// if we need to resize, double the size of the array
	if (pbuf->cur + strlen(str) >= pbuf->end)
	{
	size_t size = (pbuf->end - pbuf->start) * 2;
	pbuf->start = realloc(pbuf->start, size);
	pbuf->cur = pbuf->start + strlen(pbuf->start);
	pbuf->end = pbuf->start + size;
	}

	strcat(pbuf->start, str);
	pbuf->cur += strlen(str);
}

void flush_buffer(print_buffer *pbuf)
{
	printf("%s", pbuf->start);
}

void clear_buffer_to(print_buffer *pbuf, char *clear_to)
{
	int size = clear_to - pbuf->start;
	char* new_buf = (char*)malloc((size + 1) * sizeof(char));

	strncpy(new_buf, pbuf->start, size);
	new_buf[size] = '\0';

	free(pbuf->start);
	pbuf->start = new_buf;
}
