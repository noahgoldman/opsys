/*
 * Noah Goldman, 661058951
 *
 * All files in the archive are needed for the code to compile
 */

#include <stdio.h>
#include <dirent.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdarg.h>
#include <sys/stat.h>

#include "stats.h"
#include "path.h"
#include "buffered_print.h"

// Checks a file path to see if it is "." or ".."
int is_not_dot(char *file)
{
  return !(strcmp(file, ".") == 0 || strcmp(file, "..") == 0);
}

// Checks to see if the file type is valid
int valid_type(struct dirent *entry)
{
  int type = entry->d_type;
  return (type == DT_DIR || type == DT_LNK || type == DT_REG);
}

static int is_directory(struct dirent *entry)
{
  return entry->d_type == DT_DIR;
}

// Get the correct amount of spaces based on the recursion depth
char* get_spaces(int depth)
{
  int spaces = depth * 4;
  char *str = (char*) malloc((spaces + 1) * sizeof(char));

  int i;
  for (i = 0; i < spaces; i++)
  {
    str[i] = ' '; 
  }

  str[spaces] = '\0';

  return str;
}

// Check for a prefix match
int check_match(char *str, char *expected)
{
  if (expected == NULL) return 1;
  return strstr(str, expected) == str;
}

// Main recursion function
void read_directory(char *dir, int depth, char *str)
{
  DIR *current = opendir(dir); 

  if (current == NULL)
  {
    perror("Failed to open the specified directory");
    exit(1);
  }

  // Initialize the struct that will store our directory info.
  struct dirent *entry;

  // Store where we are beginning our print buffer
  char *buffer_index = pbuf->cur;

  int found = 0;

  // While the entry is not null, loop
  while ((entry = readdir(current))) {
    // Make sure that the file isnt "." or ".." and that it is a usable type
    if (is_not_dot(entry->d_name) && valid_type(entry)) {
      char *buf;

      // Always print out directories, but only print out files if they match the specified string
      //    if the string is null, check_match will always return true
      if (is_directory(entry) || check_match(entry->d_name, str)) {
        char *spaces = get_spaces(depth);

        buf = (char*)malloc((strlen(spaces) + strlen(entry->d_name)) * sizeof(char));

        printf("%s%s", spaces, entry->d_name);
        char *stats = print_statistics(dir, entry->d_name);

        strcat(buf, stats);

        free(spaces);
      }

      // Recurse if the file is a directory
      if (is_directory(entry)) {
        char* new_dir = join_path(2, dir, entry->d_name);
        int res = read_directory(new_dir, depth + 1, str, pbuf);
        if (res) {
          found = 1;
          flush_buffer(pbuf);
        } else {
          clear_buffer_to(pbuf, buffer_index);
        }
        free(new_dir);
      }
    }
  }

<<<<<<< HEAD
  if(closedir(current)) {
    perror("failed to close directory");
    exit(1);
  }
=======
  closedir(current);

  return found;
>>>>>>> origin/master
}

int main(int argc, char *argv[])
{
  char *cwd;

  char *str;
  if (argc == 1) {
    str = NULL;
  } else if (argc == 2) {
    str = argv[1];
  } else {
    fprintf(stderr, "Usage: ./hw1 <optional: prefix to match>");
    return EXIT_FAILURE;
  }

  cwd = getcwd(0, 0);

  if (!cwd) {
    perror("failed to get the current directory");
    return EXIT_FAILURE;
  }

  print_buffer *pbuf;
  init_buffer(pbuf);

  read_directory(cwd, 0, str, pbuf);

  free(cwd);
  return EXIT_SUCCESS;
}
