/*
 * Noah Goldman, 661058951
 */

#include <stdarg.h>
#include <string.h>
#include <stdlib.h>

#include "path.h"

int calc_size(char **chars, int n)
{
  int size = 0;
  int i;
  for (i = 0; i < n; i++)
  {
    size += strlen(chars[i]);
  } 

  size += n;
  return size;
}

char* join_path(int n, ...)
{
  va_list args;
  va_start(args, n);

  char **paths = (char**)malloc(n * sizeof(char*));
  
  int i;
  for (i = 0; i < n; i++) {
    paths[i] = va_arg(args, char*);
  }

  char* path = (char*)calloc(calc_size(paths, n), sizeof(char));

  strcpy(path, paths[0]);

  for (i = 1; i < n; i++)
  {
    strcat(path, "/");
    strcat(path, paths[i]);
  }

  free(paths);
  return path;
}
